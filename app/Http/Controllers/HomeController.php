<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;
use App\Permission;
use Illuminate\Support\Str;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;


use Illuminate\Http\Request;

class HomeController extends Controller
{
    use Notifiable, HasRoles;

    public function index(){
        $user = DB::table('users')
        ->join('article', 'article.id', '=', 'users.id')->get();

            

        

        return view('Home')->with('users', $user);

        
        // DB::table('users')->where('id', auth()->user()->id )->get();

        // if ( auth()->user()->level  == 'admin'){

        //     DB::table('model_has_roles')->insert(
            
        //         [   'role_id' => '1', 
        //             'model_id' => auth()->user()->id,  
        //          ]
        
        // );

        // }
        
        // $role = Role::findById(1);
        // $permission = Permission::findById(1);

        // $role->givePermissionTo($permission);

        //     DB::table('role_has_permissions')->insert(
            
        //         [   'permission_id' => '6',
        //             'role_id' => '3',
        //          ]
        
        // );
        
        // Role::create([
        //     'name' => 'admin'
        // ]);

        // Permission::create(['name'=> 'view post']);


       
       

    }

    public function alluser(){
        $alluser = DB::table('users')->get();
        return view('alluser',['users'=>$alluser]);
    }
    

    public function hapus ($id){
        DB::table('users')->where('id',$id)->delete();
        return redirect('/home');
    }

    public function edit ($id){
        $hasil = DB::table('users')->where('id',$id)->get();
        return view('edit',['hasil'=>$hasil]);
    }

    public function ubah (Request $req){
        DB::table('users')->where('id', $req->id)->update(

            ['name'     => $req->nama,
             'level'     => $req->role,
             'email'    => $req->email,
             'password' => $req->password
            ]
        );

        return redirect('/home');
    }

    public function post (Request $req){
        DB::table('article')->insert(
            
            [   'id'            => $req->id,
                'judul_article' => $req->title,
                'isi_article'   => $req->isi
            
             ]
    
    );
        return redirect('/home');

    }

    public function tampilarticle ($id){
        $article = DB::table('article')->where('id',$id)->get();
        return view('writer.detailarticle',['article'=>$article]);

    }

    public function tampilsemua(){
        $tampilarticle = DB::table('article')->get();
        return view('writerarticle',['article'=>$tampilarticle]);
    }

    public function hapusarticle($id_article){
        DB::table('article')->where('id_article',$id_article)->delete();
        return redirect('/writerarticle');

    }

    public function hapusarticle1($id_article){
        DB::table('article')->where('id_article',$id_article)->delete();
        return redirect('/home');
    }

    public function editarticlewriter($id_article){
        $x = DB::table('article')->where('id_article',$id_article)->get();
        return view ('editarticlewriter',['x'=>$x]);
    }

    public function ubaharticlewriter(Request $req) {
        DB::table('article')->where('id_article',$req->id)->update(

            [
                'judul_article' => $req->judul,
                'isi_article'   => $req->isi


            ]


        );
        return redirect('/home');
    }
}
