<?php

namespace App\Http\Controllers;
use Auth;
use App\User;
// use App\Role;
// use App\Permission;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class LoginController extends Controller
{

    use Notifiable, HasRoles;

    public function loginpage(){
        return view('login.login-aplikasi');
    }

    public function postlogin(Request $request){
    
        if(Auth::attempt($request->only('email','password'))){
            return redirect('/home');
        } 
            return redirect('login');
    }

    public function logout(){
        Auth::logout();
        return redirect ('login');
    }

    public function registrasi(){
        return view('login.registrasi');
    }

    public function simpanregistrasi(Request $request) {

        $user = User::create([
            'name' => $request->name,
            'level' => $request->role,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'remember_token' => Str::random(60),
        ]);
        

        //$user->assignRole('admin');

        return view('login.login-aplikasi');
    }
}
