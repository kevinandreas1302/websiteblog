<?php

namespace App;

//use App\Models\Services\Roles\Traits\HasRoles;
use App\Models\Services\Permissions\Traits\HasPermissions;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Spatie\Permission\Traits\HasRoles;


class Role extends Authenticatable
{
    use HasRoles;
    protected $fillable = [
        'id', 'name', 'guard_name',
    ];

}
