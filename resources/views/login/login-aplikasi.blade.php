<!DOCTYPE html>
<html>
<head> <title>Blog - LogIn Page</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <h1> Form Login </h1></br>
                <form class="form-horizontal" action="{{ route('postlogin') }}" method="post">
                    
                    {{ csrf_field() }}
                    <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Email : </label>
                    <div class="col-sm-10">

                    <input type="email" class="form-control" id="email" name="email" placeholder="Email"required>
</div>
</div>
           
                    <div class="form-group">
                    <label class="control-label col-sm-2" for="password">Password : </label>
                    <div class="col-sm-10">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password"required>

                   
</div>
</div>
                    <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">Log In</button>
                    <a href="/registrasi"><button type="button" class="btn btn-primary"> Belum punya akun?</button></a>
</div>
</div>
                </form>
</div>
        

    <!-- jQuery -->
    <script src="{{ asset('AdminLte/plugins/jquery/jquery.min.js') }}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{ asset('AdminLte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('AdminLte/dist/js/adminlte.min.js') }}"></script>

</body>
</html>