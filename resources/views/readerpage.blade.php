<!DOCTYPE html>
<html lang="en">
<head>
  <title>Reader Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
<h1>Latest Article</h1>
  
  <div class="row">
  @foreach($article as $a)
  <div class="col-sm-4">

      <h3>{{$a->judul_article}}</h3>
      <p>id author : {{$a->id}}</p>
      <p>{{$a->isi_article}}</p>
    </div>
  @endforeach
  </div> 
</div>



</body>
</html>