<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/registrasi','LoginController@registrasi')->name('registrasi');
Route::post('/simpanregistrasi','LoginController@simpanregistrasi')->name('simpanregistrasi');
Route::get('/login', 'LoginController@loginpage')->name('login');
Route::post('/postlogin','LoginController@postlogin')->name('postlogin');
Route::get('/logout','LoginController@logout')->name('logout');
Route::get('/cekdata','LoginController@cekdata')->name('cekdata');
//Route::get('/readerpage','HomeController@indexreader')->name('readerpage');

Route::group(['middleware' => ['auth','ceklevel:admin,writer,reader']], function () {

    Route::get('/home', 'HomeController@index')->name('home');
    //Route::get('/home', 'HomeController@indexreader')->name('home');

});

Route::group(['middleware' => ['can:publish articles']], function () {
    //
});


Route::group(['middleware' => ['auth', 'ceklevel:admin']], function(){

    Route::get('/editdata/{id}','HomeController@edit')->name('editdata');
    Route::get('/hapusdata/{id}','HomeController@hapus')->name('hapusdata');
    Route::post('/ubahdata', 'HomeController@ubah')->name('ubahdata');
    Route::get('/writerarticle','HomeController@tampilsemua')->name('writerarticle');
    Route::get('/hapusarticle/{id_article}','HomeController@hapusarticle')->name('hapusarticle');
    Route::get('/editarticlewriter/{id_article}','HomeController@editarticlewriter')->name('editarticlewriter');
    Route::post('/ubaharticlewriter','HomeController@ubaharticlewriter')->name('ubaharticlewriter');
    Route::get('/alluser','HomeController@alluser')->name('alluser');
});

Route::group(['middleware' => ['auth','ceklevel:writer']], function () {

    Route::post('/postarticle','HomeController@post')->name('postarticle');
    Route::get('/detailarticle/{id}','HomeController@tampilarticle')->name('detailarticle');
    Route::get('/hapusarticle1/{id_article}','HomeController@hapusarticle1')->name('hapusarticle1');
    Route::get('/editarticlewriter/{id_article}','HomeController@editarticlewriter')->name('editarticlewriter');
    Route::post('/ubaharticlewriter','HomeController@ubaharticlewriter')->name('ubaharticlewriter');
    
});



//----

